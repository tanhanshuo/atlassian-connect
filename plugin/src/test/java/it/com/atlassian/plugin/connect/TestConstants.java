package it.com.atlassian.plugin.connect;

public class TestConstants
{
    public static final String XML_ADDON_PUBLIC_KEY = "-----BEGIN PUBLIC KEY-----\n" +
            "                MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtWghUS3KRC01kJkeU83P\n" +
            "                S2CSH6Vma2twv7p7JVHUIWl69tNPvn46mYt3gbFQgmBv74pAJJkb1FvBNxhy0B19\n" +
            "                9fXgZUVS+6R7597t0hVG610Zbl4Ar7xvs/h3ACwUhSib3ad496nghLvOnarrJIgw\n" +
            "                sNQeCaBz+FczCYQAt7Mk8vGjE1XSha6FEBNKIYijgoZhqLGcTxxyBIDmMwyNU2Oz\n" +
            "                lknyFZ1ZhYIPKuw069njJjbuL0Nv9PXKO+pNZMQvwo7WrepJ0B0VG5KYjDzetFzx\n" +
            "                FOtr7x6M5Z7XZsPhNIjDjE+oBHu+Hzc1/bGyWpZrzc04nqCNTsmOvID1H50Jt778\n" +
            "                mQIDAQAB\n" +
            "                -----END PUBLIC KEY-----";
    public static final String XML_ADDON_PRIVATE_KEY = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEpQIBAAKCAQEAtWghUS3KRC01kJkeU83PS2CSH6Vma2twv7p7JVHUIWl69tNP\n" +
            "vn46mYt3gbFQgmBv74pAJJkb1FvBNxhy0B199fXgZUVS+6R7597t0hVG610Zbl4A\n" +
            "r7xvs/h3ACwUhSib3ad496nghLvOnarrJIgwsNQeCaBz+FczCYQAt7Mk8vGjE1XS\n" +
            "ha6FEBNKIYijgoZhqLGcTxxyBIDmMwyNU2OzlknyFZ1ZhYIPKuw069njJjbuL0Nv\n" +
            "9PXKO+pNZMQvwo7WrepJ0B0VG5KYjDzetFzxFOtr7x6M5Z7XZsPhNIjDjE+oBHu+\n" +
            "Hzc1/bGyWpZrzc04nqCNTsmOvID1H50Jt778mQIDAQABAoIBAQCLTi6fn1E/L5R9\n" +
            "uQfQBTEVylAMG0DeZsBLi5G7o+4Jxm2WE8meGGM5vB8GqjqQFCyBP6JoOGdlmRx0\n" +
            "CcNJTAyJj8pFGopSEgrQkaIBfTNb1L+NwIQ4b7U7+CayLCeJ5hhji5LaZUqzw2E0\n" +
            "NKekAy2Y7Rsv+1ZzM8tOmF7QsrJCGL8/WOq7QLVmuMKawOeGzp7H6RQ9BPjOBB/Q\n" +
            "swe2G5uzLY/SkTlVjEriVGuHP1NzX2t7QzV1HpLtVhLrCBKZvo1LJ9sfSXxOZmM3\n" +
            "/uCNE3+JMaTsMk0DSdZnY4pwfOPOfbUAf0TBHAKIu2styybPS46RPGRPDbxhkCTP\n" +
            "Wc1P7kCxAoGBAOl3I9n8NXwKfG30Z9KENgTXHYPvVdJKhcpRiqklq7KKv6oqmp4Z\n" +
            "0/A44whbaWl4P0ugmYsRifDJpVNE0C0Mw0nzWpuq84y6/GeU/VP+M+B17/yDMElj\n" +
            "3EXITNM2XrZlD6iYI6WleaNasKW5M1pVOhGiHdVTQxTRWm+eZYfiYmLtAoGBAMbq\n" +
            "ownQ1mDogKZUol5zq09z/6NeXFZpVhDNiaQzuX2J8wBh9Bkb9njtNPyg5+XoCw/c\n" +
            "0/UsplTSCEKxDWFHTOKCAEiqA7HQfHbqpykwX76DNnilCIKa1AXhVeUmu/+YYaOO\n" +
            "SAuyHOZlbKJaK8mjk4EEe1Mr39z537elsh1JDS7dAoGBAOQFqi14yKA6+abG5DRX\n" +
            "Tw9RLxGyS4cVpDCjjaOBGH5MR8Ci1dr+7OIeHZgG+CC8Ak4SMIUEf05/FAsNFao6\n" +
            "Ye6zUVbjE/bqliVw/i/wAqkDZ36gfyPe9b/uTyKnYsAQWsfWuFJMGU6z//4MsZxT\n" +
            "y2B3j13QcZ8+jm6gLRgXwvJNAoGATEF/JzAsPxJi32DqrhLhxZ/OjK6L74SKPf7N\n" +
            "mWlK3tmXkrn6ffW+UzV8bqywue5u7zHU/9SSH0o1aHu/iV9wFhWITlL+/5fRXzUt\n" +
            "yBiHW92pcC60SH1acraj2ykyQRYFuFG/RNyPP7P6JXMz/iT7UyaIsKXNOEWCgkC/\n" +
            "O4LZzvECgYEA4HaAE2ne4Wjevnsz8tQm97Of5XImPNI1EcIPH8shpsxB88ubw0hr\n" +
            "KUXwyj0dMvq7mB+8nJL9Hg/QWcqJ+buLRPcoqqaveRYXrVSnT+9MnYQ5LzqRdH3b\n" +
            "QBUnpzF6+17WlvPbE4t/OD0wzP6mUPh8UOgsU2bb+cVv4cMbroujLq8=\n" +
            "-----END RSA PRIVATE KEY-----";
    public static final String XML_ADDON_RESOURCE_PATH = "/com/atlassian/connect/xml_oauth_descriptor.xml";
}
