package com.atlassian.plugin.connect.plugin.capabilities.provider;

import com.atlassian.plugin.connect.modules.beans.BlueprintModuleBean;

public interface BlueprintModuleProvider extends ConnectModuleProvider<BlueprintModuleBean> {

}
