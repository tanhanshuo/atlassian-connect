package com.atlassian.plugin.connect.plugin.module.websection;

import com.atlassian.plugin.web.descriptors.WebSectionModuleDescriptor;

public interface ProductSpecificWebSectionModuleDescriptorFactory
{
    WebSectionModuleDescriptor createWebSectionModuleDescriptor();
}
