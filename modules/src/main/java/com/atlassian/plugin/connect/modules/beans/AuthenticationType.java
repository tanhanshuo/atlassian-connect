package com.atlassian.plugin.connect.modules.beans;

public enum AuthenticationType
{
    OAUTH, JWT, NONE
}
